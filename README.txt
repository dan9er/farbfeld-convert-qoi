farbfeld-convert-qoi
====================

farbfeld[0] to/from Quite Ok Image[1] converters.
Originally made as my CS50x 2021 final project.

Features
--------
-	Just over 400 cloc(1) lines of C99
-	Linear runtime
-	No dynamic memory usage

Anti-Features
-------------
-	Always outputs the following metadata in the header:
	-	Channels: RBGA
	-	Colorspace: sRGB with linear alpha
-	Refuses to continue if a non-8-bit subpixel is encountered, rather than
	silently quantize away percision (Explicitly use a "16 to 8" filter first)

Building & Installation
-----------------------
Copy config.def.mk to config.mk, then edit that to match your local setup using
your text editor of choice. Then:

	make
	make install

(Of course, install with root permissions if needed)

Usage
-----
Invoke as you would with png2ff(1), ff2png(1), et al.:

	$qoi | qoi2ff | $farbfeld

	$farbfeld | ff2qoi | $qoi

Bugs
----
qoi2ff always dies with an unexpected EOF error, even though the output is
correct (lel(1) reads the farbfeld data just fine).

Repository
----------
	git clone https://gitlab.com/dan9er/farbfeld-convert-qoi.git

[0]: https://tools.suckless.org/farbfeld/
[1]: https://qoiformat.org/
