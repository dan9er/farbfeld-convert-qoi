/* See LICENSE.txt and LICENSE_UTIL.txt for copyright & license details. */
#include <stdint.h>
#include <stdio.h>

#define LEN(x) (sizeof (x) / sizeof *(x))
#define htonll(x) ((((uint64_t)htonl(x)) << 32) + htonl((x) >> 32))

/* chunk tag masks */
#define QOI_OP_RGB   UINT8_C(0xfe) /* 0b11111110 */
#define QOI_OP_RGBA  UINT8_C(0xff) /* 0b11111111 */
#define QOI_OP_INDEX UINT8_C(0x00) /* 0b00****** */
#define QOI_OP_DIFF  UINT8_C(0x40) /* 0b01****** */
#define QOI_OP_LUMA  UINT8_C(0x80) /* 0b10****** */
#define QOI_OP_RUN   UINT8_C(0xc0) /* 0b11****** */

typedef struct {
	uint_least8_t r, g, b, a;
} rgba8;

extern char *argv0;

void warn(const char *, ...);
void die(const char *, ...);

void ff_read_header(uint32_t *restrict width, uint32_t *restrict height);
void ff_write_header(const uint32_t width, const uint32_t height);

int fshut(FILE *restrict, const char *restrict);

void efread(void *restrict, const size_t, const size_t, FILE *restrict);
void efwrite(const void *restrict, const size_t, const size_t, FILE *restrict);
void efputs(const char *restrict, FILE *restrict);
void efputc(const int, FILE *);

inline uint8_t
qoi_hash(const rgba8 p)
{
	return (   ((uint16_t)p.r *  3)
	         + ((uint16_t)p.g *  5)
	         + ((uint16_t)p.b *  7)
	         + ((uint16_t)p.a * 11)
	       ) % 64;
}
