# See LICENSE.txt for copyright & license details.

.POSIX:

include config.mk

REQ = util
BIN = ff2qoi qoi2ff
MAN = $(BIN:=.1)

all: $(BIN)

ff2qoi: ff2qoi.o $(REQ:=.o)
qoi2ff: qoi2ff.o $(REQ:=.o)

ff2qoi.o: ff2qoi.c config.mk $(REQ:=.h)
qoi2ff.o: qoi2ff.c config.mk $(REQ:=.h)

.o:
	$(CC) -o $@ $(LDFLAGS) $< $(REQ:=.o)

.c.o:
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $<

clean:
	rm -f $(BIN) $(BIN:=.o) $(REQ:=.o)

dist:
	rm -rf "farbfeld-converter-qoi-$(VERSION)"
	mkdir -p "farbfeld-converter-qoi-$(VERSION)"
	cp -R LICENSE_UTIL.txt LICENSE.txt Makefile README.txt config.def.mk \
	      $(BIN:=.c) $(REQ:=.c) $(REQ:=.h) $(MAN) WINDOWS.txt \
	      "farbfeld-converter-qoi-$(VERSION)"
	tar -cf - "farbfeld-converter-qoi-$(VERSION)" | gzip -c \
		> "farbfeld-converter-qoi-$(VERSION).tar.gz"
	rm -rf "farbfeld-converter-qoi-$(VERSION)"

install: all
	mkdir -p "$(DESTDIR)$(PREFIX)/bin"
	cp -f $(BIN) "$(DESTDIR)$(PREFIX)/bin"
	for f in $(BIN); do chmod 755 "$(DESTDIR)$(PREFIX)/bin/$$f"; done
	mkdir -p "$(DESTDIR)$(MANPREFIX)/man1"
	cp -f $(MAN) "$(DESTDIR)$(MANPREFIX)/man1"
	for m in $(MAN); do chmod 644 "$(DESTDIR)$(MANPREFIX)/man1/$$m"; done

uninstall:
	for f in $(BIN); do rm -f "$(DESTDIR)$(PREFIX)/bin/$$f"; done
	for m in $(MAN); do rm -f "$(DESTDIR)$(MANPREFIX)/man1/$$m"; done
