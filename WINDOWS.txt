1.	Change all arpa/inet.h includes to winsock2.h

2. Delete the htonll macro definition in util.h

3.	Add includes to io.h and fcntl.h in ff2qoi.c and qoi2ff.c, and add these
	lines before `ff_read_header` to set stdin/out to binary mode:

		_setmode(_fileno(stdout),_O_BINARY);
		_setmode(_fileno(stdin ),_O_BINARY);

4.	Invoke the compiler & linker. How to do so exactly depends on your setup:
	-	MSYS2, Cygwin, WSL, MinGW: Edit config.mk and run make as in README.txt.
	-	Directly run cl/clang/clang-cl.

You'll need to pass WinSock2 to the linker:
C:\Program Files (x86)\Windows Kits\*\Lib\*\um\*\WS2_32
