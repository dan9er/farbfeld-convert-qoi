/* See LICENSE.txt for copyright & license details. */
#include <arpa/inet.h>

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"

static void ff_write_pixel8(const rgba8 p);
static void ff_write_pixel8n(const rgba8 p, uint8_t n);

static inline void qoi_read_header(uint32_t *restrict width,
	uint32_t *restrict height);

static inline void qoi2ff(const uint32_t width, const uint32_t height);

static inline void usage(void);

static void
ff_write_pixel8(const rgba8 p)
{
	uint_least64_t rgba16buf;

	memset(&rgba16buf, 0x0000000000000000, sizeof(uint_least64_t));

	rgba16buf = htonll(
		(	  ((uint64_t)p.r << 48)
			| ((uint64_t)p.g << 32)
			| ((uint64_t)p.b << 16)
			|  (uint64_t)p.a
		) * 0x101 /* x |= x << 8 */
	);

	efwrite(&rgba16buf, 2, 4, stdout);
}

static void
ff_write_pixel8n(const rgba8 p, uint8_t n)
{
	uint_least64_t rgba16buf;

	memset(&rgba16buf, 0x0000000000000000, sizeof(uint_least64_t));

	rgba16buf = htonll(
		(	  ((uint64_t)p.r << 48)
			| ((uint64_t)p.g << 32)
			| ((uint64_t)p.b << 16)
			|  (uint64_t)p.a
		) * 0x101 /* x |= x << 8 */
	);

	for (uint8_t i = 0; i < n; ++i) {
		efwrite(&rgba16buf, 2, 4, stdout);
	}
}

static inline void
qoi_read_header(uint32_t *restrict width, uint32_t *restrict height)
{
	uint_least32_t header[3];

	efread(header, sizeof(*header), LEN(header), stdin);

	if (memcmp("qoif", header, sizeof("qoif") - 1))
		die("Invalid magic value");

	*width  = ntohl(header[1]);
	*height = ntohl(header[2]);

	/* ignore channels & colorspace */
	efread(header, 2, 1, stdin);
}

static inline void
qoi2ff(const uint32_t width, const uint32_t height)
{
	const uint64_t totalpix = (uint64_t)width * (uint64_t)height;

	     uint64_t pix = 0;
	        rgba8 curr = {0x00,0x00,0x00,0xff},
	              index[64];
	uint_least8_t readbuf[8];
	       int8_t scratch;

	ff_write_header(width, height);

	memset(index, 0x00000000, sizeof(index));

	while (pix < totalpix) {
		efread(&readbuf, sizeof(uint_least8_t), 1, stdin);

		switch (readbuf[0]) {
		case QOI_OP_RGB:
			efread(&curr, sizeof(uint_least8_t), 3, stdin);
		break;

		case QOI_OP_RGBA:
			efread(&curr, sizeof(uint_least8_t), 4, stdin);
		break;

		default:
			switch (readbuf[0] & QOI_OP_RUN) {

			case QOI_OP_INDEX:
				curr = index[readbuf[0] & ~QOI_OP_RUN];
			goto next_noindex;

			case QOI_OP_DIFF:
				curr.r += (int8_t)((readbuf[0] & (QOI_OP_RUN >> 2)) >> 4) - 2;
				curr.g += (int8_t)((readbuf[0] & (QOI_OP_RUN >> 4)) >> 2) - 2;
				curr.b += (int8_t) (readbuf[0] & (QOI_OP_RUN >> 6))       - 2;
			break;

			case QOI_OP_LUMA:
				efread(&readbuf[1], sizeof(uint_least8_t), 1, stdin);

				/* scratch as diffg */
				scratch = (int8_t)(readbuf[0] & ~QOI_OP_RUN) - 32;
				curr.g += scratch;

				curr.r += (int8_t)((readbuf[1] & 0xf0) >> 4) - 8 + scratch;
				curr.b += (int8_t) (readbuf[1] & 0x0f)       - 8 + scratch;
			break;

			case QOI_OP_RUN:
				/* scratch as run length */
				scratch = (readbuf[0] & ~QOI_OP_RUN) + 1;
				ff_write_pixel8n(curr, scratch);
				pix += scratch;
			goto next_nowrite;
			}
		}

		index[qoi_hash(curr)] = curr;

	next_noindex:
		ff_write_pixel8(curr);
		++pix;

	next_nowrite:
		;
	}

	efread(readbuf, sizeof(uint_least8_t), 8, stdin);
	if (memcmp("\x0\x0\x0\x0\x0\x0\x0\x1", readbuf, 8))
		die("Invalid end marker");
}

static inline void
usage(void)
{
	fprintf(stderr, "usage: %s", argv0);
	exit(1);
}

int
main(int argc, char *argv[])
{
	uint32_t width, height;

	argv0 = argv[0];
	if (argc != 1)
		usage();

	qoi_read_header(&width, &height);

	qoi2ff(width, height);

	return fshut(stdout, "<stdout>");
}
